#include "Arduino.h"
#include <SPI.h>
#include <RF24.h>
#include <stdlib.h>
//#include "SevenSegmentTM1637.h"
//#include "SevenSegmentExtended.h"

/* initialize global TM1637 Display object
 *  The constructor takes two arguments, the number of the clock pin and the digital output pin:
 *  SevenSegmentTM1637(byte pinCLK, bye pinDIO)
 */
//const byte PIN_CLK = 2; // define CLK pin (any digital pin)
//const byte PIN_DIO = 3; // define DIO pin (any digital pin)
//SevenSegmentExtended  display(PIN_CLK, PIN_DIO);


// This is just the way the RF24 library works:
// Hardware configuration: Set up nRF24L01 radio on SPI bus (pins 10, 11, 12, 13) plus pins 7 & 8
RF24 radio(7, 8);

byte addresses[][6] = {"1Node","2Node"};
byte count = 0;
int relayPin = 2;

// -----------------------------------------------------------------------------
// SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP
// -----------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  Serial.println("THIS IS THE RECEIVER CODE - YOU NEED THE OTHER ARDUINO TO TRANSMIT");

  // Initiate the radio object
  radio.begin();

  // Set the transmit power to lowest available to prevent power supply related issues
  radio.setPALevel(RF24_PA_MAX);

  // Set the speed of the transmission to the quickest available
  radio.setDataRate(RF24_2MBPS);

  // Use a channel unlikely to be used by Wifi, Microwave ovens etc
  radio.setChannel(124);

  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1, addresses[1]);

  // Start the radio listening for data
  radio.startListening();
//  display.begin();
//  display.setBacklight(100);

  pinMode(relayPin, OUTPUT);
  
}

// -----------------------------------------------------------------------------
// We are LISTENING on this device only (although we do transmit a response)
// -----------------------------------------------------------------------------
void loop() {
  // This is what we receive from the other device (the transmitter)
  float data;
  
    // Go and read the data and put it into that variable
  if (radio.available()) {
    while(radio.available()){
      radio.read( &data, sizeof(float));
    }
    Serial.println("Turning ON relay");
    digitalWrite(relayPin, LOW);
    count = 0;
   
  // No more data to get so send it back.
  // First, stop listening so we can talk
  radio.stopListening();
  radio.write( &data, sizeof(float) );
  // Now, resume listening so we catch the next packets.
  radio.startListening();
  // Tell the user what we sent back (the random numer + 1)
  //Serial.print("Sent response ");
  }//end if(radio.available())
  Serial.print("Data ");
  Serial.println(data);
  Serial.print("Count ");
  Serial.println(count);
 
  
  count++;
  if (count > 5) {
    Serial.println("Turning OFF relay");
    digitalWrite(relayPin, HIGH);
    count = 6;
  }
  delay(1000);
}
